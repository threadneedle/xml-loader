## Solidatus XML loader

Loads an xml file to Solidatus as a model.

Usage:
```
node src/index load --xml file.xml --model "My new model" --token <token> --host <host>
```