const ModelFragment = require('./ModelFragment')
const convert = require('xml-js')
const _ = require('lodash')
const fs = require('fs')

const addChildrenToModelFragment = (element, parent, modelFragment, feedback) => {
  _.each(element.attributes, (value, key) => {
    modelFragment.addAttribute({
      parent,
      name: `@${key}`,
      properties: {
        'SOL.Type': 'XML Attribute',
        Value: value
      }
    })
  })

  _.each(element.elements, child => {
    if (child.type !== 'element') {
      return
    }

    const attr = modelFragment.addAttribute({
      parent,
      name: child.name,
      properties: {
        'SOL.Type': 'XML Element',
        Value:
          (child.elements &&
            child.elements.find(element => element.type === 'text') &&
            child.elements.find(element => element.type === 'text').text) ||
          ''
      }
    })

    addChildrenToModelFragment(child, attr, modelFragment, feedback)
  })
}

const addToModelFragment = (xmljs, modelFragment, feedback) => {
  const root = _.find(xmljs.elements, e => e.type === 'element')
  const object = modelFragment.addObject({
    name: root.name,
    properties: {
      'SOL.Type': 'XML File'
    }
  })

  modelFragment.addRoot(object)

  addChildrenToModelFragment(root, object, modelFragment, feedback)
}

const convertXml = (xml, modelFragment, feedback) => {
  const xmljs = convert.xml2js(xml, {trim: true})
  addToModelFragment(xmljs, modelFragment, feedback)
}

const convertFile = (file, modelFragment) => {
  const data = fs.readFileSync(file)

  convertXml(data, modelFragment)
}

const process = (data, callback) => {
  const _modelFragment = new ModelFragment()

  try {
    if (data.isRaw) {
      convertXml(data.data, _modelFragment)
      callback()
    } else if (data.isLocalFile) {
      const file = data.data
      convertFile(file, _modelFragment)
    }
  } catch (error) {
    console.error(error)
    return
  }

  return _modelFragment.model
}

module.exports = process
