const uuid = require('uuid/v4')

class ModelFragment {
  constructor() {
    this.model = {
      version: 'simple',
      roots: [],
      entities: {}
    }
  }

  addRoot(data) {
    const root = this.addObject({
      name: 'XML file',
      children: [data.id]
    })

    this.model.roots.push(root.id)
  }

  addObject(data) {
    data.id = uuid()
    data.children = data.children || []

    this.model.entities[data.id] = data

    return data
  }

  addAttribute(data) {
    const parent = data.parent
    const attr = {
      name: data.name,
      properties: data.properties
    }

    const entity = this.addObject(attr)

    this.model.entities[parent.id].children.push(entity.id)

    return entity
  }
}

module.exports = ModelFragment
