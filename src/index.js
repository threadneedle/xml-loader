const process = require('./XMLImporter')
const fetch = require('node-fetch')
const yargs = require('yargs')
const fs = require('fs')

// Extract arguments from the command line
const argv = yargs
  .usage('Usage: node index.js <command> [arguments...]')
  .command('load', 'Convert XML file to a Solidatus model', yargs => {
    return yargs
      .option('xml', {
        demandOption: true,
        describe: 'XML file',
        type: 'string'
      })
      .option('host', {
        demandOption: true,
        describe: 'URL of solidatus instance',
        type: 'string'
      })
      .option('token', {
        demandOption: true,
        describe: 'Solidatus API token',
        type: 'string'
      })
      .option('model', {
        demandOption: true,
        describe: 'Name of new model or existing model',
        type: 'string'
      })
  })
  .example('$0 load --xml file.xml --model "My new model" --token <token> --host <host>')
  .demandCommand()
  .help().argv

//Import the XML file and convert to a Solidatus model
const model = process({isLocalFile: true, data: argv.xml})
//Write the model to a file and to the console
fs.writeFileSync('output.json', JSON.stringify(model))
console.log(JSON.stringify(model))

// Create a JavaScript object representing a ReplaceModel command using the model above
command = {
  model: model,
  cmd: 'ReplaceModel',
  comparator: {
    id: true
  }
}

// Build a request containing the command above
request = {
  commit: true,
  cmds: [command]
}

// Create the headers for our API requests
headers = {
  'Content-Type': 'application/json',
  Authorization: 'Bearer ' + argv.token
}

// Find a model using a search term
fetch(argv.host + '/api/v1/models/?searchTerm=' + encodeURI(argv.model), {
  method: 'get',
  headers: headers
})
  .then(response => {
    if (!response.ok) {
      throw Error(response.statusText)
    }
    return response.json()
  })
  .then(json => {
    if (json.items.length > 0) {
      return json.items[0]
    } else {
      // No model can be found, Create a new model with an API request
      return fetch(argv.host + '/api/v1/models', {
        method: 'post',
        headers: headers,
        body: JSON.stringify({name: argv.model})
      })
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText)
          }
          return response.json()
        })
        .catch(error => console.error(error))
    }
  })
  .catch(function (error) {
    console.error(error)
  })
  // Then use the request created earlier to replace the contents of the found/created model with the model built earlier, getting the ID of the new model from the response JSON of the create model request
  .then(function (responseJson) {
    fetch(argv.host + '/api/v1/models/' + responseJson.id + '/update', {
      method: 'post',
      headers: headers,
      body: JSON.stringify(request)
    })
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText)
        }
      })
      .catch(error => console.error(error))
  })
